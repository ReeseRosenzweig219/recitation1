/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package recitation1;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

/**
 *
 * @author reese
 */
public class Recitation1 extends Application {
    
    @Override
    public void start(Stage primaryStage) {
        Button btn = new Button();
        btn.setText("Say 'Hello World'");
        
        Button b_two_n = new Button();
        b_two_n.setText("Say 'Goodbye Cruel World!'");
        
        btn.setOnAction(new EventHandler<ActionEvent>() {
            
            @Override
            public void handle(ActionEvent event) {
                System.out.println("Hello World!");
            }
        });
        
        b_two_n.setOnAction(new EventHandler<ActionEvent>() {
            
            @Override
            public void handle(ActionEvent event) {
                System.out.println("Goodbye Cruel World!");
            }
        });
        
        HBox hbox = new HBox();
        //StackPane rootTwo = new StackPane();
        hbox.getChildren().addAll(btn, b_two_n);
        //root.getChildren().add(b_two_n);
        
        Scene scene = new Scene(hbox, 300, 250);
        //Scene sceneTwo = new Scene(rootTwo, 300, 450);
        
        primaryStage.setTitle("Hello World!");
        primaryStage.setScene(scene);
        //primaryStage.setScene(sceneTwo);
        primaryStage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
